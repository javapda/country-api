# country-api #

* Promise based api for retrieving country information

# Installation / setup #
```
npm install country-api --save
```

# Usage #

## node ##
```
const {CountryApi} = require('country-api')

new CountryApi().byNamePrefix('mex')
.then((data) => console.log(data))

```
## output
```
[ { name: 'Mexico',
    topLevelDomain: [ '.mx' ],
    alpha2Code: 'MX',
    alpha3Code: 'MEX',
    callingCodes: [ '52' ],
    capital: 'Mexico City',
    altSpellings:
     [ 'MX',
       'Mexicanos',
       'United Mexican States',
       'Estados Unidos Mexicanos' ],
    region: 'Americas',
    subregion: 'Central America',
    population: 122273473,
    latlng: [ 23, -102 ],
    demonym: 'Mexican',
    area: 1964375,
    gini: 47,
    timezones: [ 'UTC-08:00', 'UTC-07:00', 'UTC-06:00' ],
    borders: [ 'BLZ', 'GTM', 'USA' ],
    nativeName: 'México',
    numericCode: '484',
    currencies: [ [Object] ],
    languages: [ [Object] ],
    translations:
     { de: 'Mexiko',
       es: 'México',
       fr: 'Mexique',
       ja: 'メキシコ',
       it: 'Messico',
       br: 'México',
       pt: 'México',
       nl: 'Mexico',
       hr: 'Meksiko',
       fa: 'مکزیک' },
    flag: 'https://restcountries.eu/data/mex.svg',
    regionalBlocs: [ [Object], [Object] ],
    cioc: 'MEX' } ]

```

# API # 

* all
* byNamePrefix
* byNameFull
* byCallingCode
* byCapitalCity
* byCurrencyCode
* byIso3166Code
* byIso3166CodeList
* byIso6391LanguageCode
* byRegion
* byRegionalBlock

# Resources #

* [examples on git repository](doc/index.md)
* [restcountries](https://restcountries.eu)
* [axios](https://github.com/axios/axios)
* [ISO 639-1 Language Codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
* [country calling codes](https://en.wikipedia.org/wiki/List_of_country_calling_codes)
