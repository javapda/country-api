const CountryApi = require('./country-api')

const testAll=() =>
new CountryApi()
// .all()
.all('name')//,'capital','currencies')
.then((result)=>{
    for ( var i = 0; i < result.length; i++) {
        const item=result[i]
        console.log(item.name);    
    }
    console.log(result.length);

})
// .then((result)=>console.log(result.length))

const testByNamePrefix=() =>
new CountryApi()
.byNamePrefix('mex')
.then((result)=>result)

const testByNameFull=() =>
new CountryApi()
.byNameFull('mexico')
.then((result)=>result)

const testByIso3166Code=() =>
new CountryApi()
.byIso3166Code('co')
.then((result)=>result)

const testByIso3166CodeList=() =>
new CountryApi()
.byIso3166CodeList('co','no')
.then((result)=>result)

const testByCurrencyCode=() =>
new CountryApi()
.byCurrencyCode('cop')
.then((result)=>result)

const testByIso6391LanguageCode=() =>
new CountryApi()
.byIso6391LanguageCode('es')
.then((result)=>result)

const testByCapitalCity=() =>
new CountryApi()
.byCapitalCity('washington')
.then((result)=>result)

const testByCallingCode=() =>
new CountryApi()
.byCallingCode('372')
.then((result)=>result)

const testByRegion=() =>
new CountryApi()
.byRegion('oceania')
.then((result)=>result)

const testByRegionalBlock=() =>
new CountryApi()
.byRegionalBlock('EU')
.then((result)=>result)

// testByRegionalBlock().then(r=>console.log(r))
// testByRegion().then(r=>console.log(r))
//testByCallingCode().then(r=>console.log(r))
//testByCapitalCity().then(r=>console.log(r))
//testByIso6391LanguageCode().then(r=>console.log(r))
//testByNamePrefix().then(r=>console.log(r))
// testByNameFull().then(r=>console.log(r[0]))
// testByIso3166Code().then(r=>console.log(r))
// testByIso3166CodeList().then(r=>console.log(r))
// testByCurrencyCode().then(r=>console.log(r))
testAll()