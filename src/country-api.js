const axios = require('axios')
module.exports = class CountryApi {
    constructor() {
        this.baseUrl = `https://restcountries.eu/rest/v2`
    }

    async all() {
        var args = Array.prototype.slice.call(arguments);
        const props = {params:{fields:args.join(";")}}
        const url = `${this.baseUrl}/all`
        return (await axios(url,props)).data
    }
    async byCallingCode(callingCode) {
        const url = `${this.baseUrl}/callingcode/${callingCode}`
        return (await axios(url)).data
    }
    async byCapitalCity(capitalCity) {
        const url = `${this.baseUrl}/capital/${capitalCity}`
        return (await axios(url)).data
    }
    async byCurrencyCode(currencyCode) {
        const url = `${this.baseUrl}/currency/${currencyCode}`
        return (await axios(url)).data
    }

    async byIso3166Code(code) {
        const url = `${this.baseUrl}/alpha/${code}`
        return (await axios(url)).data
    }
    async byIso3166CodeList(/* any number of codes */) {
        var args = Array.prototype.slice.call(arguments);
        const props = {params:{codes:args.join(";")}}
        const url = `${this.baseUrl}/alpha`
        return (await axios(url,props)).data
    }
    async byIso6391LanguageCode(languageCode) {
        const url = `${this.baseUrl}/lang/${languageCode}`
        return (await axios(url)).data
    }
    async byNamePrefix(name) {
        const url = `${this.baseUrl}/name/${name}`
        return (await axios(url)).data
    }
    async byNameFull(name) {
        const url = `${this.baseUrl}/name/${name}`
        const props = {params: {fullText:true}}
        return (await axios(url,props)).data
    }
    async byRegion(region) {
        const validRegions = ['africa', 'americas', 'asia', 'europe', 'oceania']
        if (!validRegions.includes(region.toLowerCase())) {
            throw `invalid region "${region}" - must be one of ${validRegions.join()}`
        }
        const url = `${this.baseUrl}/region/${region}`
        return (await axios(url)).data
    }
    async byRegionalBlock(regionalBlockCode) {
        const validRegionalBlocks = ['EU','EFTA','CARICOM','PA','AU','USAN','EEU','AL','ASEAN','CAIS','CEFTA','NAFTA','SAARC']
        if (!validRegionalBlocks.includes(regionalBlockCode.toUpperCase())) {
            throw `invalid regionalBlock "${regionalBlockCode}" - must be one of ${validRegionalBlocks.join()}`
        }
        const url = `${this.baseUrl}/regionalbloc/${regionalBlockCode}`
        return (await axios(url)).data
    }
}
