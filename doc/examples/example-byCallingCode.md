# example all #

## code ##
```
const {CountryApi} = require('country-api')

new CountryApi().byCallingCode('372')
.then((data) => console.log(data))

```
## output ##
```
[ { name: 'Estonia',
    topLevelDomain: [ '.ee' ],
    alpha2Code: 'EE',
    alpha3Code: 'EST',
    callingCodes: [ '372' ],
    capital: 'Tallinn',
    altSpellings: [ 'EE', 'Eesti', 'Republic of Estonia', 'Eesti Vabariik' ],
    region: 'Europe',
    subregion: 'Northern Europe',
    population: 1315944,
    latlng: [ 59, 26 ],
    demonym: 'Estonian',
    area: 45227,
    gini: 36,
    timezones: [ 'UTC+02:00' ],
    borders: [ 'LVA', 'RUS' ],
    nativeName: 'Eesti',
    numericCode: '233',
    currencies: [ [Object] ],
    languages: [ [Object] ],
    translations: 
     { de: 'Estland',
       es: 'Estonia',
       fr: 'Estonie',
       ja: 'エストニア',
       it: 'Estonia',
       br: 'Estônia',
       pt: 'Estónia',
       nl: 'Estland',
       hr: 'Estonija',
       fa: 'استونی' },
    flag: 'https://restcountries.eu/data/est.svg',
    regionalBlocs: [ [Object] ],
    cioc: 'EST' } ]

```
