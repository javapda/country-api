# examples #

* [all](./examples/example-all.md)
* [byNamePrefix](./examples/example-byNamePrefix.md)
* [byCapitalCity](./examples/example-byCapitalCity.md)
* [byCallingCode](./examples/example-byCallingCode.md)
* [byRegion](./examples/example-byRegion.md)